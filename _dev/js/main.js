function mobileMenu(){
    var mobileBtn = $('.js-mobile'),
        mobileMenu = $('.js-mobile-menu'),
        mobileLink = $('.js-mobile-link');

    $(mobileBtn).click(function(){
        $(this).toggleClass('open');
        $(mobileMenu).toggleClass('is-open');

        if (mobileMenu.hasClass('is-open')){
            $('body' ).append( "<div class='fade'></div>" );
        }
        else {
            $('div.fade').remove();
        }
    })

    $(mobileLink).click(function(){
        if (mobileMenu.hasClass('is-open')){
            $('div.fade').remove();
            $(mobileBtn).removeClass('open');
            $(mobileMenu).removeClass('is-open');
        }
    })
}

function scrollInit(){
    $("body").on('click', '[href*="#"]', function(e){
        var fixed_offset;

        if ($(window).width() < 768){
            fixed_offset = 76;
        }
        else{
            fixed_offset= 0;
        }

        $('html,body').stop().animate({ scrollTop: $(this.hash).offset().top - fixed_offset }, 1000);
        e.preventDefault();
    });
}

$(document).ready(function(){
    mobileMenu();
    scrollInit();
});